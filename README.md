# docker-postgresql

Docker container with postgresql 9.5.4, runs under centos 7.2 1511.  
usage:  
`docker run -d --name db --restart unless-stopped -p 5432:5432 bsarda/postgresql:9.5
docker run -d --name db --restart unless-stopped -p 45432:5432 --env ADMIN_USER=su --env ADMIN_PASSWORD='P@ssw0rd!' bsarda/postgresql:9.5
`
the data location is /var/lib/pgsql/9.5/data inside the container and exported as a volume.  

Sample usage:  
`docker exec db su postgres -c 'psql -c "\du"'  
psql -h localhost -p 5432 -c "\du" -U su  
`
